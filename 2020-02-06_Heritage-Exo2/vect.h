#pragma once
class vect
{
protected:
    int nelem; // nombre d'�l�ments
    int* adr; // adresse zone dynamique contenant les �l�ments

public:
    vect(int); // constructeur
    
    ~vect(); // destructeur

    int& operator [] (int); // acc�s � un �l�ment par son "indice"
};