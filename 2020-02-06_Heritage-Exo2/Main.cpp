#include <iostream>
#include "vectok.h"

using namespace std;

int main()
{
    void fct(vectok);
    int nI;

    vectok vect(6);
    for (nI = 0; nI < vect.taille(); nI++) {
        vect[nI] = nI;
    }

    cout << "vecteur vect : ";
    for (nI = 0; nI < vect.taille(); nI++) {
        cout << vect[nI] << " ";
    }
    cout << "\n";


    vectok vect2(3);
    vect2 = vect;
    cout << "vecteur vect2 : ";
    for (nI = 0; nI < vect2.taille(); nI++) {
        cout << vect2[nI] << " ";
    }
    cout << "\n";

    //fct(vect);

    return 0;
}
/*void fct(vectok vect)
{
    cout << "vecteur re�u par fct : " << "\n";
    int nI;
    for (nI = 0; nI < vect.taille(); nI++) cout << vect[nI] << " ";
}*/