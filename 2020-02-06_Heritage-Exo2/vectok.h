#pragma once
#include "vect.h"

class vectok : public vect
{ // pas de nouveaux membres donn�e

public:
    //Constructeur
    vectok(int dim) : vect(dim) {}
    //Contructeur par recopie
    vectok(vectok&);
    //Surdefinition de &
    vectok& operator = (vectok&);
    
    //Fonction taille
    int taille()
    {
        return nelem;
    }
};


//Contructeur de recopie
vectok::vectok(vectok& vect) : vect(vect.nelem)
{
    int nCpt;
    for (nCpt = 0; nCpt < nelem; nCpt++) adr[nCpt] = vect.adr[nCpt];
}


//Surdefinition op�rateur &
vectok & vectok::operator = (vectok & vect)
{
    if (this != &vect)
    {
        delete adr;
        adr = new int[nelem = vect.nelem];
        int i;
        for (i = 0; i < nelem; i++) adr[i] = vect.adr[i];
    }
    return (*this);
}