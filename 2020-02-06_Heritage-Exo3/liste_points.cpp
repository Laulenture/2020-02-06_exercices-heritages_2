#include "liste_points.h"

void liste_points::affiche()
{
    point* ptr = (point*)premier();
    while (!fini()) {
        ptr->affiche(); ptr = (point*)prochain();
    }
}