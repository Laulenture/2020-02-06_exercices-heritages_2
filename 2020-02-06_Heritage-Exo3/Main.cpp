#include <iostream>
#include "liste_points.h"

using namespace std;

int main()
{
    liste_points listePoint;
    point point1(420, 69), point2(66, 42), point3(9, 11);
    cout << "Point1: \n"; listePoint.ajoute(&point1); listePoint.affiche(); cout << "\n";
    cout << "Point2: \n"; listePoint.ajoute(&point2); listePoint.affiche(); cout << "\n";
    cout << "Point3: \n"; listePoint.ajoute(&point3); listePoint.affiche(); cout << "\n";

	return 0;
}