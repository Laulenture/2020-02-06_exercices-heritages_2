#include "liste.h"
#include <stdlib.h>

//Methodes
void liste::ajoute(void* chose)
{
    element* tempElem = new element;
    tempElem->elemSuivant = elemDebut;
    tempElem->contenu = chose;
    elemDebut = tempElem;
}

void* liste::premier() // positionne sur premier �l�ment
{
    elemCurrent = elemDebut;
    if (elemCurrent != NULL) return (elemCurrent->contenu);
    else return NULL;
}

void* liste::prochain() // positionne sur prochain �l�ment
{
    if (elemCurrent != NULL)
    {
        elemCurrent = elemCurrent->elemSuivant;
        if (elemCurrent != NULL) return (elemCurrent->contenu);
    }
    return NULL;
}


int liste::fini() {
    return (elemCurrent == NULL);
}

//Destructeur
liste::~liste()
{
    element* suiv;
    elemCurrent = elemDebut;
    while (elemCurrent != NULL)
    {
        suiv = elemCurrent->elemSuivant; delete elemCurrent; elemCurrent = suiv;
    }
}

