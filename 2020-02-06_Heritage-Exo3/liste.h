#pragma once
#include <stdlib.h>

struct element // structure d'un �l�ment de liste
{
    element* elemSuivant; // pointeur sur l'�l�ment elemSuivant
    void* contenu; // pointeur sur un objet quelconque
};

class liste
{
    element* elemDebut; // pointeur sur premier �l�ment
    element* elemCurrent; // pointeur sur �l�ment elemCurrent

public:
    //Constructeur
    liste() 
    {
        elemDebut = NULL;
        elemCurrent = elemDebut;
    }

    //Methodes
    void ajoute(void* chose); // ajoute un �l�ment en d�but de liste

    void* premier(); // positionne sur premier �l�ment

    void* prochain(); // positionne sur prochain �l�ment

    int fini();

    //Destructeur
    ~liste();
};
