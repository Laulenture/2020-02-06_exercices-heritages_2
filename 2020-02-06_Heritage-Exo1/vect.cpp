#include "vect.h"
#include <cstdlib>

//Constructeur
vect::vect(int nI)
{
	nelem = nI;
	adr = (int*)malloc(nI+1);
}

//Destructeur
vect::~vect()
{
	free(adr);
}

//surcharge opérateur &
int& vect::operator[](int nI)
{
	return adr[nI];
}
