#pragma once
#include "vect.h"
class vectb : public vect
{
    int nDebut, nFin;

public:

    vectb(int debut, int fin) : vect(fin - debut + 1)
    {
        nDebut = debut; nFin = fin;
    }
    int& operator [] (int nCpt)
    {
        return vect::operator [] (nCpt - nDebut);
    }
};