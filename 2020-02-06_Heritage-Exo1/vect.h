#pragma once
class vect
{
    int nelem; // nombre d'�l�ments
    int* adr; // adresse zone dynamique contenant les �l�ments
public:
    
    //Constructeur
    vect(int nI); 
    
    //Destructeur
    ~vect();

    int& operator [] (int); // acc�s � un �l�ment par son "indice"
};